## Установка
1. Установить [Python 3.7](https://www.python.org/downloads/). (с 3.8 не тестировалось, но по идее тоже должно работать)
2. Установить [MongoDB](https://www.mongodb.com/download-center/community).
3. Установить зависимости. 
    ```
    pip install -r requirements.txt
    ```
4. Создать папку `config` и в ней создать файл `config.yml`, где нужно указать собственный IP для сервера. Полный список параметров конфигурации можно найти в [public_data_server\config.py](./public_data_server/config.py).
    ```yaml
    http_server:
      host: {IP}
    searching_server:
      host: {IP}  
    ```
5. В файле [server_watcher.py](./server_watcher.py) вписать выбранный IP и порт (`8080` по умолчанию) в строку `url`.
    ```python   
    url = 'http://{IP}:8080/'   
    ```
## Использование
1. Для запуска есть несколько вариантов:
    1. Запустить напрямую [main.py](./public_data_server/main.py). Рекомендуется для отладки.
    2. Запустить через [server_watcher](./server_watcher.py), который будет перезапускать сервер, если тот подвиснет больше, чем на 2 секунды.
    3. Запустить через [run.bat](./run.bat), который в свою очередь запускает Server Watcher.
2. По умолчанию HTTP-интерфейс запускается на `8080` порту, а интерфейс для связи с диспетчером на порту `8001`.
3. Справка по протоколу запросов доступна по [ссылке](https://drive.google.com/open?id=1dfayKGyVSMk28kovCZNYA-Qrd-f4ncql).
