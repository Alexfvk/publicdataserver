import errno
import os

import phonenumbers
import pymongo
from PIL import Image, UnidentifiedImageError
from aiohttp import web
from aiohttp.web import middleware
from loguru import logger

from config import config
from errors import PnValidationError
from open_source_data_provider import OpenSourceDataProvider, UpdateMode

routes = web.RouteTableDef()

log_main = logger.bind(tag='MAIN')


def validate_pn(pn):
    if not pn:
        raise PnValidationError("Empty Value")
    res = pn.strip()
    if len(res) and res[0] != '+':
        res = '+' + res
    try:
        pn = phonenumbers.parse(res)
        is_pn_valid = phonenumbers.is_possible_number(pn)
        if is_pn_valid:
            res = phonenumbers.format_number(pn, phonenumbers.PhoneNumberFormat.E164)
        else:
            raise PnValidationError(message="Use international (E164) phone number format for target pn")
    except phonenumbers.NumberParseException:
        raise PnValidationError(message="Use international (E164) phone number format for target pn")
    return res


@middleware
async def error_middleware(request, handler):
    try:
        response = await handler(request)
        if response.status == 200:
            return response
        status = response.status
        message = response.message
    except web.HTTPException as ex:
        message = ex.reason
        status = ex.status
    except pymongo.errors.PyMongoError as ex:
        log_main.error(ex)
        message = "Database Error"
        status = 500
    except FileNotFoundError as ex:
        message = "Media Not Found"
        status = 404
    # except Exception as ex:
    #     message = "Server Error"
    #     status = 500
    response = {
        "status": "error",
        "error": {
            "msg": message
        }
    }
    log_main.warning(f'RESP to {request.remote} with {status} {response}')
    return web.json_response(status=status, data=response)


@routes.get('/')
async def hello(request):
    return web.Response(text="Server is online")


@routes.get('/props')
async def get_props(request):
    log_main.info(f'{request.method} props '
                  f'from {request.remote}')
    return web.Response(text="Not Implemented Yet")


@routes.get('/info')
async def get_info(request):
    log_main.info(f'{request.method} info: '
                  f'target_type = {request.query.get("target_type", "None")}, '
                  f'target_value = {request.query.get("target_value", "None")}, '
                  f'services = {request.query.get("services", "None")}, '
                  f'force_update = {request.query.get("force_update", "None")}, '
                  f'data = {request.query.get("data", "None")}, '
                  f'history = {request.query.get("history", "None")} '
                  f'from {request.remote}')
    if "target_type" not in request.query or "target_value" not in request.query or "services" not in request.query:
        raise web.HTTPBadRequest(reason="Parameters target_type, target_value and services are mandatory")

    target = {
        "type": request.query["target_type"],
        "value": request.query["target_value"]
    }

    # PN validation
    if target["type"] == 'pn':
        try:
            target["value"] = validate_pn(target["value"])
        except PnValidationError as e:
            log_main.error(f'PN Validation: {e.message} ({target["value"]})')
            raise web.HTTPBadRequest(reason=e.message)
        log_main.info(f'Use PN={target["value"]} for request')

    services = request.query.get("services", "").lower().split(",")
    update_mode = UpdateMode.OBSOLETE
    if request.query.get("force_update", False):
        update_mode = UpdateMode.ALL
    need_history = request.query.get("history", False)

    request_id = app["open_source_data_provider"].request(target, services, update_mode=update_mode,
                                                          need_history=need_history)
    result = await app["open_source_data_provider"].get_result(request_id, data=True, history=need_history)
    log_main.info(f'RESP to {request.remote} with request {result["id"]} {result["status"]}')
    return web.json_response(data=result)


@routes.get('/result')
async def get_result(request):
    log_main.info(f'{request.method} result: '
                  f'id = {request.query.get("id", "None")}, '
                  f'data = {request.query.get("data", "None")}, '
                  f'history = {request.query.get("history", "None")}, '
                  f'from {request.remote}')
    if "id" not in request.query:
        raise web.HTTPBadRequest(reason="Parameter id is mandatory")

    need_history = request.query.get("history", False)
    result = await app["open_source_data_provider"].get_result(request.query["id"], data=True, history=need_history)

    if result:
        log_main.debug(f'RESP to {request.remote} with request {result["id"]} {result["status"]}')
        return web.json_response(data=result)
    else:
        raise web.HTTPNotFound(reason="ID is not found")


@routes.get('/services')
async def get_services(request):
    log_main.info(f'{request.method} services '
                  f'from {request.remote}')
    services = app["open_source_data_provider"].searcher.available_services()
    log_main.info(f'RESP to {request.remote} with {services}')
    return web.json_response(services)


@routes.get('/media/{filename}')
async def get_media(request):
    log_main.info(f'{request.method} media/{request.match_info["filename"]} '
                  f'from {request.remote}')
    filename = config["file_path"] + "\\" + request.match_info["filename"]
    if os.path.isfile(filename):
        log_main.info(f'RESP to {request.remote} with file {filename}')
        return web.FileResponse(filename)
    else:
        log_main.warning(f'File {filename} for {request.remote} is not found')
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)


@routes.get('/media/thumbnail/{filename}')
async def get_media(request):
    log_main.info(f'{request.method} media/thumbnail/{request.match_info["filename"]} '
                  f'from {request.remote}')
    filename = config["file_path"] + f'\\{request.match_info["filename"]}'
    if os.path.isfile(filename):
        thumbnail_filename = config["file_path"] + f'\\thumbnail_{request.match_info["filename"]}'
        if os.path.isfile(thumbnail_filename):
            log_main.info(f'RESP to {request.remote} with file {thumbnail_filename}')
            return web.FileResponse(thumbnail_filename)
        else:
            log_main.info(f'Generating thumbnail {thumbnail_filename}')
            if os.path.isfile(filename):
                try:
                    im = Image.open(filename)
                    im.thumbnail((config["thumbnail_size"]["height"], config["thumbnail_size"]["width"]))
                    im.save(thumbnail_filename)
                    log_main.info(f'RESP to {request.remote} with file {thumbnail_filename}')
                    return web.FileResponse(thumbnail_filename)
                except UnidentifiedImageError as ex:
                    log_main.warning(f'Cannot open {filename}')
                    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)
            else:
                log_main.warning(f'File {filename} for {request.remote} is not found')
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)

    else:
        log_main.warning(f'File {filename} for {request.remote} is not found')
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)


async def on_startup(app):
    log_main.info(f'Starting Open Source Data Provider')
    app["open_source_data_provider"] = OpenSourceDataProvider()


if __name__ == '__main__':
    # configure logging
    log_main.configure(**config["log"])
    if config["log"]["extra"]["aiomsg_log"]:
        log_main.enable("aiomsg")

    app = web.Application(middlewares=[error_middleware])
    app.add_routes(routes)
    app.on_startup.append(on_startup)
    log_main.info(f'Starting HTTP Server on {config["http_server"]["host"]}:{config["http_server"]["port"]}')
    web.run_app(app, host=config["http_server"]["host"], port=config["http_server"]["port"])
