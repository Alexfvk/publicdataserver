from __future__ import annotations

import asyncio
import datetime
import math
import operator
import os
import uuid
from pathlib import Path
from typing import List, Dict

from aiomsg.ack_protocol import AckProtoIncomingRequest, AckProtoCommunication
from aiomsg.aiomsg import Connection
from aiomsg.websocket import WebSocketServer
from loguru import logger

import errors
from config import config
from os_db import OsDb

log_search_serv = logger.bind(tag='SEARCH')


class SearchingStatus:
    NEW = "new"
    PENDING = "pending"
    COMPLETED = "completed"
    ERROR = "error"


class WorkerStatus:
    BUSY = 'busy'
    READY = 'ready'


def exists(file):
    """Check does file exist and his size is not null"""
    return os.path.exists(file) and os.path.getsize(file) > 0


def is_timeout(timestamp, interval):
    return datetime.datetime.now() - timestamp >= datetime.timedelta(seconds=interval)


class WorkerRequest:
    def __init__(self, db, owner, worker, target, service_name,
                 response_timeout=config["searching_server"]["response_timeout"]):
        self.id: str = str(uuid.uuid4())
        self.owner = owner
        self.worker = worker
        self.target: dict = target
        self.service_name = service_name
        self.status = SearchingStatus.NEW
        self.service_data = []
        self.service_data_index = {}
        self.error: Exception = None
        self.start_time: datetime.datetime = None
        self.finish_time: datetime.datetime = None
        self.db = db
        self.response_timeout = response_timeout

        self.save()

    def serialize(self) -> dict:
        res = {
            "id": self.id,
            "worker_id": self.worker.unit_id,
            "target": self.target,
            "service_name": self.service_name,
            "status": self.status,
            "start_time": self.start_time.timestamp() if self.start_time else None,
            "finish_time": self.finish_time.timestamp() if self.finish_time else None,
            "service_data": self.serialize_service_data()  # TODO get rid of duplicate service data in all tables
        }
        if self.error:
            res['error'] = self.error
        return res

    def serialize_service_data(self) -> dict:
        return {
            'target': self.target,
            'service_name': self.service_name,
            'service_data': self.service_data,
            'timestamp': self.finish_time.timestamp() if self.finish_time else None,
            'index': self.service_data_index
        }

    def save(self):
        self.db.update_worker_request(self)

    async def get_files(self, worker: Worker, files: List[str]):
        tasks = []
        if not os.path.exists(config["file_path"]):
            os.mkdir(config["file_path"])
        for file_name in files:
            tasks.append(asyncio.create_task(self.get_file(worker, file_name)))
        await asyncio.gather(*tasks, return_exceptions=True)

    async def get_file(self, file):
        full_path_name = Path(config["file_path"]) / file
        if not exists(full_path_name):
            with open(full_path_name, 'wb') as f:
                file = await self.worker.request_file(file_name=file)
                f.write(file)

    async def run(self):
        try:
            self.status = SearchingStatus.PENDING
            self.start_time = datetime.datetime.now()
            self.worker.requests[self.id] = self
            self.save()
            log_search_serv.debug(f'Worker {self.worker.unit_id}: Request target={self.target} service={self.service_name}')
            data = await self.worker.request(method='find',
                                             args={'target': self.target, 'service_name': self.service_name},
                                             timeout=self.response_timeout)
            if data['files']:
                log_search_serv.debug(
                    f'Worker {self.worker.unit_id}: Get files={data["files"]} for target={self.target} service={self.service_name}')
                await self.get_files(self.worker, data['files'])
            self.on_response(data)
        except errors.BaseError as e:
            log_search_serv.debug(e)
            self.on_error(e)
        except Exception as e:
            log_search_serv.exception('Unexpected error while searching')
            log_search_serv.debug(e)
            self.on_error(e)
        finally:
            self.worker.requests.pop(self.id, None)
            self.finish_time = datetime.datetime.now()
            if self.status == SearchingStatus.COMPLETED:
                self.db.save_os_data(self.serialize_service_data())
            self.save()
            log_search_serv.debug(
                f'Worker {self.worker.unit_id}: Request target={self.target} service={self.service_name} completed')

    def on_response(self, msg):
        self.status = SearchingStatus.COMPLETED
        self.service_data = msg['service_data']
        # self.service_data_index = msg['index']
        self.service_data_index = {}
        self.save()

    def on_error(self, e: Exception):
        self.status = SearchingStatus.ERROR
        self.error = errors.serialize(e)
        self.save()


class SearchingTask:
    def __init__(self, owner: SearchingServer, target: dict, service: str, db: OsDb) -> None:
        self.id: str = str(uuid.uuid4())
        self.owner = owner
        self.worker_request = None
        self.target: dict = target
        self.service_name = service
        self.status = SearchingStatus.NEW
        self.task: asyncio.Task = None
        self.search_completed = asyncio.Event()
        self.error: Exception = None
        self.db = db
        self.start_time: datetime.datetime = None
        self.finish_time: datetime.datetime = None

    def result(self) -> dict:
        if self.error:
            raise errors.SearchingError('Error while searching', self.error)
        if self.worker_request:
            return self.worker_request.serialize_service_data()
        else:
            return {
                'target': self.target,
                'service_name': self.service_name,
                'service_data': [],
                'timestamp': self.finish_time.timestamp() if self.finish_time else None,
                'index': {}
            }

    def serialize(self) -> dict:
        res = {
            "id": self.id,
            "target": self.target,
            "service_name": self.service_name,
            "status": self.status,
            "worker_id": self.worker_request.worker.unit_id if self.worker_request else None,
            "start_time": self.start_time.timestamp() if self.start_time else None,
            "finish_time": self.finish_time.timestamp() if self.finish_time else None,
        }
        if self.error:
            res['error'] = self.error
        return res

    async def run(self):
        not_found_worker_error_raised = False
        while not self.search_completed.is_set():
            try:
                if is_timeout(self.start_time, config["searching_server"]["overall_timeout"]):
                    raise errors.OverallTimeout()
                with self.owner.find_ready_worker(self.service_name, self.target['type']) as worker:
                    self.worker_request = WorkerRequest(self.db, self, worker, self.target, self.service_name)
                    self.owner.db.update_searching_task(self)
                    await self.worker_request.run()
                    if self.worker_request.status == SearchingStatus.COMPLETED:
                        self.on_response()
                    elif self.worker_request.status == SearchingStatus.ERROR:
                        self.on_error(self.worker_request.error)
            except errors.OverallTimeout as e:
                log_search_serv.debug(e)
                self.on_error(e)
                self.on_finish()
            except errors.NotFoundWorkerError as e:
                if not not_found_worker_error_raised:
                    log_search_serv.debug(e)
                    not_found_worker_error_raised = True
                self.on_error(e)
                await asyncio.sleep(config["searching_server"]["not_found_worker_attempt_pause"])
                continue
            except errors.BaseError as e:
                log_search_serv.debug(e)
            except Exception as e:
                log_search_serv.exception('Unexpected error while searching')
                log_search_serv.debug(e)
                self.on_error(e)

            if not self.search_completed.is_set():
                await asyncio.sleep(config["searching_server"]["attempt_pause"])

    def on_response(self):
        self.status = SearchingStatus.COMPLETED
        self.on_finish()

    def on_error(self, e: Exception):
        self.status = SearchingStatus.ERROR
        self.error = errors.serialize(e)

    def on_finish(self):
        self.finish_time = datetime.datetime.now()
        self.search_completed.set()
        self.owner.remove_search(self.id)

    async def wait_completed(self, timeout=None):
        await asyncio.wait_for(self.search_completed.wait(), timeout)

    def begin(self):
        self.status = SearchingStatus.PENDING
        self.start_time = datetime.datetime.now()
        self.owner.add_search(self)
        self.task = asyncio.create_task(self.run())
        return self.task


class Worker(AckProtoCommunication):
    def __init__(self, owner, socket):
        super().__init__(Connection(socket))
        self.owner = owner
        self.services = []
        self.status = WorkerStatus.BUSY
        self.unit_id = ''
        self.requests = {}

    async def on_request(self, req: AckProtoIncomingRequest):
        try:
            if req.method == 'connect':
                if self.owner.get_client(req.args["unit_id"]):
                    raise errors.NotUniqueUnitIdError(req.args["unit_id"])
                log_search_serv.info(f'New client connected {req.args["unit_id"]}')
                self.unit_id = req.args['unit_id']
                if 'services' in req.args:
                    self.services = req.args['services']
                await req.reply_ack()
        except errors.NotUniqueUnitIdError as e:
            await req.reply_error(e.to_dict())
            await self.close()

    async def on_disconnect(self, e: Exception):
        log_search_serv.info(f'Client {self.unit_id} disconnected')

    def preprocess(self, msg):
        if isinstance(msg['body'], dict):
            if 'services' in msg['body']:  # update service info
                for service in msg['body']['services']:
                    for s in self.services:
                        if service["name"] == s["name"]:
                            self.services.remove(s)
                            self.services.append(service)
                            break
            if 'status' in msg['body']:
                self.status = msg['body']['status']
                log_search_serv.debug(f'Worker {self.unit_id} is {self.status}')

    async def request_file(self, file_name: str):
        log_search_serv.debug(f'Request file {file_name}')
        return await self.request(method='get_file', args={'file_name': file_name})

    def __enter__(self):
        log_search_serv.debug(f'Worker {self.unit_id} is locked')
        self.status = WorkerStatus.BUSY  # only setting, without clearing on exit,
        # because it will be changed by first message from worker
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class SearchingServer(WebSocketServer):
    def __init__(self, host, port, db):
        super().__init__(host, port)
        self.requests: Dict[str, SearchingTask] = {}
        self.db = db

    def create_client(self, socket):
        return Worker(owner=self, socket=socket)

    def get_client(self, unit_id):
        for client in self.clients:
            if client.unit_id == unit_id:
                return client
        return False

    def available_services(self):
        """return service list"""
        res = {}
        for w in self.clients:
            for service in w.services:
                if service['name'] not in res:
                    res[service['name']] = {
                        'target_types': set()
                    }
                res[service['name']]['target_types'].update(service['target_types'])
        for r in res.values():
            r['target_types'] = list(r['target_types'])
        return res

    def is_service_available(self, service_name, target_type):
        services = self.available_services()
        return service_name in services and target_type in services[service_name]['target_types']

    # TODO: Extract to utils
    def to_timestring(self, value):
        dt_str = ""
        if value:
            dt_str = value.strftime('%Y-%m-%d %H:%M:%S')
            dt_str += f'.{math.floor(value.microsecond / 1000)}'
        return dt_str

    def dispatchers_info(self):
        dispatchers = []
        for d in self.clients:
            dispatcher = {
                'unit_id': d.unit_id,
                'status': d.status,
                'services': d.services,
                'tasks': [],
                'task_history': self.db.get_searching_task_history(d.unit_id)
            }
            for t in d.searching_tasks.values():
                task = {
                    'id': t.id,
                    'target': t.target,
                    'service_name': t.service_name,
                    'status': t.status,
                    'service_data': t.service_data,
                    'error': {},
                    'start_time': self.to_timestring(t.start_time),
                    'finish_time': self.to_timestring(t.finish_time)
                }
                if t.error:
                    task['error'] = t.error
                    task['error'].pop('traceback', None)
                dispatcher['tasks'].append(task)
            dispatchers.append(dispatcher)
        return dispatchers

    async def search(self, target, service) -> dict:
        if not self.is_service_available(service, target["type"]):
            log_search_serv.warning(f"No worker for {service}-{target['type']}")
            raise errors.ServiceNotSupportError(service)
        req = self.find_current_search(target, service)
        if not req:
            req = SearchingTask(owner=self, target=target, service=service, db=self.db)
            log_search_serv.info(f'Searching Task for {service}-{target["type"]} created with id {req.id}')
            req.begin()
        try:
            await req.wait_completed(
                config["searching_server"]["overall_timeout"] + config["searching_server"]["response_timeout"])
        except asyncio.TimeoutError as e:
            log_search_serv.error(f'Timeout for task {req.id}')
            log_search_serv.debug(e)
            req.on_error(e)
        self.db.update_searching_task(req)
        log_search_serv.info(
            f'Searching Task {req.id} for {req.service_name}-{target["type"]} completed with status {req.status}')
        return req.result()

    def add_search(self, req):
        self.requests[req.id] = req
        self.db.update_searching_task(req)

    def find_current_search(self, target, service):
        for s in self.requests.values():
            if s.target == target and s.service_name == service:
                return s

    def remove_search(self, id_):
        try:
            self.requests.pop(id_)
        except KeyError:
            log_search_serv.debug(f"Service request id={id_} does not exist")

    def find_ready_worker(self, service_name, target_type):
        workers = []
        for w in self.clients:
            if w.status == WorkerStatus.READY:
                for service in w.services:
                    if service['name'] == service_name and target_type in service['target_types'] \
                            and service['potential'] > 0:  # TODO is_ready
                        workers.append((w, service['potential']))
            workers = sorted(workers, key=operator.itemgetter(1))
            if workers:
                return workers[0][0]
        raise errors.NotFoundWorkerError(service_name, target_type)
