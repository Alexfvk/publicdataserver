import asyncio
from loguru import logger

from searching_server import SearchingServer
from client_request import ClientRequest, UpdateMode

from os_db import OsDb
from config import config


class OpenSourceDataProvider:
    def __init__(self):
        self.db = OsDb()
        self.client_requests = {}
        self.searcher = SearchingServer(config["searching_server"]["host"], config["searching_server"]["port"], self.db)
        self.logger = logger.bind(tag='DATAPROV')
        self.logger.info(f'Starting Searching Server on {config["searching_server"]["host"]}:{config["searching_server"]["port"]}')
        asyncio.create_task(self.searcher.run())

    def request(self, target, services, *, need_cache=True, need_history=True, update_mode=UpdateMode.OBSOLETE) -> str:
        req = ClientRequest(self.searcher, self.db, target, services, need_cache=need_cache,
                            need_history=need_history, update_mode=update_mode)
        self.client_requests[req.id] = (req, asyncio.create_task(self.process_request(req)))
        return req.id

    async def process_request(self, client_request):
        try:
            self.logger.info(f'Processing request {client_request.id}')
            await client_request.execute()
        finally:
            self.logger.info(f'Request {client_request.id} completed')
            self.client_requests.pop(client_request.id, None)

    async def get_result(self, id_, data, history):
        result = self.db.get_client_request(id_, data=data, history=history)
        self.logger.debug(f'Get result for {id_} - {result}')
        return result
