import collections
import sys
import yaml
import os
import re
from loguru import logger

this_dir = os.path.dirname(os.path.abspath(__file__))

config = {
    "http_server": {
        "host": '127.0.0.1',
        "port": 8080
    },
    "searching_server": {
        "host": 'localhost',
        "port": 8001,
        "overall_timeout": 150,
        "response_timeout": 150,
        "attempt_pause": 1,
        "not_found_worker_attempt_pause": 0
    },
    "request_obsoletion_time": {
        "default": 86400,
    },
    "thumbnail_size": {
        "height": 200,
        "width": 200
    },
    "file_path": './osfiles',
    "log": {
        "levels": [
            {'name': 'ERROR', 'color': '<red>'},
            {'name': 'WARNING', 'color': '<light-yellow>'},
            {'name': 'DEBUG', 'color': '<magenta>'},
            {'name': 'INFO', 'color': '<light-cyan>'}
        ],
        "handlers": [
            {"sink": sys.stderr, "level": 'INFO', "backtrace": False, "diagnose": False,
             "format": '<yellow>{time:HH:mm:ss.SSS}</yellow> '
                       '| <level>{level:^7}</level> '
                       '| <cyan>{extra[tag]:^8}</cyan>'
                       '| <level>{message}</level>'},
            {"sink": "../logs/{time:YYYY-MM-DD}.log", "rotation": "00:00", "encoding": "utf8",
             "format": '<yellow>{time:HH:mm:ss.SSS}</yellow> '
                       '| <level>{level:^7}</level> '
                       '| <cyan>{extra[tag]:^8}</cyan>'
                       '| <level>{message}</level>'},
            {"sink": "../logs/errors_{time:YYYY-MM-DD}.log", "level": 'ERROR', "rotation": "00:00", "encoding": "utf8",
             "format": '<yellow>{time:HH:mm:ss.SSS}</yellow> '
                       '| <level>{level:^7}</level> '
                       '| <cyan>{extra[tag]:^8}</cyan>'
                       '| <level>{message}</level>'}
        ],
        "extra": {
            "aiomsg_log": False,
            "tag": "UNKNOWN"
        }
    }

}


# Recursive config update
def update_config(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update_config(d.get(k, {}), v)
        else:
            d[k] = v
    return d


try:
    with open(os.path.normpath(this_dir + "/../config/config.yml"), 'r') as stream:
        new_config = yaml.safe_load(stream)
        update_config(config, new_config)
except IOError:
    logger.configure(**config["log"])
    with logger.contextualize(tag='CONFIG LOADER'):
        logger.warning("Config error. Use default configuration")

# Replace sys.stdout with object for logging and relative path with abs path
for handler in config["log"]["handlers"]:
    if handler["sink"] == "sys.stdout":
        handler["sink"] = sys.stdout
    elif handler["sink"] == "sys.stderr":
        handler["sink"] = sys.stderr
    elif isinstance(handler["sink"], str) and re.match(r'\.+/', handler["sink"]):  # relative path
        handler["sink"] = os.path.normpath(this_dir + "/" + handler["sink"])
if re.match(r'\.+/', config["file_path"]):  # relative path
    config["file_path"] = os.path.normpath(this_dir + "/" + config["file_path"])
