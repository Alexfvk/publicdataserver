import asyncio
import uuid
from datetime import datetime
from enum import auto

from loguru import logger

from errors import BaseError


class UpdateMode:
    NO = auto()
    ALL = auto()
    OBSOLETE = auto()


class RequestStatus:
    NEW = "new"
    PENDING = "pending"
    COMPLETED = "completed"
    ERROR = "error"


class ClientRequest:
    def __init__(self, searcher, db, target, services, *, need_cache, need_history, update_mode):
        self.pending_update_task = None
        self.searcher = searcher
        self.db = db
        self.need_cache = need_cache
        self.need_history = need_history
        self.update_mode = update_mode

        self.id = str(uuid.uuid4())
        self.target = target
        self.services = services
        self.timestamp = datetime.now().timestamp()
        self.error = {}
        self.full_report = self.init_service_reports()

        self.logger = logger.bind(tag='CLIENT')

        self.save()

    def init_service_reports(self):
        reports = {}
        for name in self.services:
            report = {
                'service_name': name,
                'status': RequestStatus.PENDING,
                'actual': None,
                'history': []
            }
            reports[name] = report

            # read from db if not need updating
            if not self.need_update(name) and self.need_cache:
                report['actual'] = self.read_actual(name)
                if self.need_history:
                    report['history'] = self.read_history(name)
                report['status'] = RequestStatus.COMPLETED
        return reports

    def read_actual(self, service_name):
        return self.db.get_last_target_data(self.target, service_name)

    def read_history(self, service_name):
        return self.db.get_all_target_data(self.target, service_name)

    def need_update(self, service_name):
        if self.update_mode == UpdateMode.NO:
            return False
        if self.update_mode == UpdateMode.ALL:
            return True
        if self.update_mode == UpdateMode.OBSOLETE and self.db.is_obsolete(self.target, service_name):
            return True
        return False

    async def execute(self):
        pending = set()
        for name, report in self.full_report.items():
            if report['status'] == RequestStatus.PENDING:
                pending.add(self.update(name))
        self.logger.debug(f'Request {self.id}: {len(pending)} pending service requests')
        for f in asyncio.as_completed(pending):
            await f
            self.save()
        self.logger.debug(f'Request {self.id}: All service requests are completed. Request status - {self.get_status()}')

    async def update(self, service_name):
        try:
            self.full_report[service_name]['actual'] = await self.searcher.search(self.target, service_name)
            self.logger.info(f'Request {self.id}-{service_name}: Completed')
            self.logger.debug(f'Request {self.id}-{service_name}: '
                         f'Actual Data - {self.full_report[service_name]["actual"]}')
        except BaseError as e:
            e_dict = e.to_dict()
            e_dict.pop('traceback', None)
            self.full_report[service_name]['error'] = e_dict
            self.logger.warning(f'Request {self.id}-{service_name}: '
                           f'Completed with error - {self.full_report[service_name]["error"]}')
            if self.need_cache:
                self.full_report[service_name]['actual'] = self.read_actual(service_name)

        if self.need_history:
            self.full_report[service_name]['history'] = self.read_history(service_name)
        if self.full_report[service_name]['actual']:
            self.full_report[service_name]['status'] = RequestStatus.COMPLETED
        else:
            self.full_report[service_name]['status'] = RequestStatus.ERROR

    def serialize(self):
        res = {
            "id": self.id,
            "status": self.get_status(),
            "target": self.target,
            "services": self.services,
            "timestamp": self.timestamp,
            "data": list(self.full_report.values())
        }
        if self.error:
            res['error'] = self.error
        return res

    def save(self):
        self.db.save_client_request(self)

    def get_status(self):
        if any(r['status'] == RequestStatus.PENDING for r in self.full_report.values()):
            return RequestStatus.PENDING
        elif any(r['status'] == RequestStatus.COMPLETED for r in self.full_report.values()):
            return RequestStatus.COMPLETED
        else:  # all(r['status'] == RequestStatus.ERROR for r in self.full_report.values()):
            return RequestStatus.ERROR
