import math
from typing import List, Optional
import pymongo
from datetime import datetime
from config import config


class OsDb:
    def __init__(self):
        self.client = pymongo.MongoClient()
        self.db = self.client.public_data_server_db
        self.searching_tasks = self.db.searching_tasks
        self.service_data = self.db.service_data
        self.requests = self.db.requests
        self.worker_requests = self.db.worker_requests

    def save_os_data(self, service_data):
        self.service_data.insert_one(service_data)

    def is_obsolete(self, target, service_name):
        last_data = self.get_last_target_data(target, service_name)
        if not last_data:
            return True
        time_since_start = datetime.now().timestamp() - last_data["timestamp"]
        obs_time = config["request_obsoletion_time"].get("service_name", None)
        if not obs_time:
            obs_time = config["request_obsoletion_time"]["default"]
        return time_since_start > obs_time

    def save_client_request(self, client_request):
        obj = client_request.serialize()
        self.db.client_request.update({'id': obj['id']}, obj, upsert=True)

    def update_searching_task(self, searching_task):
        d = searching_task.serialize()
        self.searching_tasks.replace_one({'id': d["id"]}, d, upsert=True)

    def update_worker_request(self, worker_request):
        d = worker_request.serialize()
        self.worker_requests.replace_one({'id': d["id"]}, d, upsert=True)

    def get_searching_task_history(self, worker_id=None, task_count=10):
        req = {"worker_id": worker_id} if worker_id else None
        data = self.searching_tasks.find(req, sort=[("start_time", pymongo.DESCENDING)]).limit(task_count)
        res = []
        for datum in data:
            del datum['_id']
            res.append(datum)
        return res

    def get_last_target_data(self, target, service) -> Optional[dict]:
        data = self.service_data.find_one({
            "target.type": target["type"],
            "target.value": target["value"],
            "service_name": service},
            sort=[("timestamp", pymongo.DESCENDING)]
        )
        if data:
            del data['_id']
            return data

    def get_all_target_data(self, target, service) -> List[dict]:
        data = self.service_data.find({
            "target.type": target["type"],
            "target.value": target["value"],
            "service_name": service},
            sort=[("timestamp", pymongo.DESCENDING)]
        )
        res = []
        for datum in data:
            del datum['_id']
            res.append(datum)
        return res

    def get_client_request(self, request_id, data=True, history=True):  # TODO removing data and history from result
        request = self.db.client_request.find_one({'id': request_id})
        if request:
            request.pop("_id", None)
        return request

    def get_request_page_count(self, requests_per_page):
        count = self.db.client_request.count_documents({})
        return math.ceil(count / requests_per_page)

    def get_request_history(self, requests_per_page, page=1):
        skips = requests_per_page * (page - 1)
        cursor = self.db.client_request \
            .find(sort=[("timestamp", pymongo.DESCENDING)]) \
            .skip(skips).limit(requests_per_page)
        return [x for x in cursor]
