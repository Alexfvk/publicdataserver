import traceback
from typing import Union


def serialize(exc: Exception):
    if hasattr(exc, 'to_dict'):
        return exc.to_dict()
    else:
        return {
            'type': exc.__class__.__name__,
            'message': str(exc),
            'traceback': ''.join(traceback.format_tb(exc.__traceback__))
        }


class BaseError(Exception):
    def __init__(self, message: str, details: dict = {}):
        super().__init__(message)
        self.message = message
        self.details = details

    def to_dict(self):
        e = {'type': self.__class__.__name__,
             'message': self.message,
             'traceback': ''.join(traceback.format_tb(self.__traceback__))
             }
        if self.details:
            e['details'] = self.details
        return e


class WrapperError(BaseError):
    def __init__(self, message: str, exc: Union[Exception, dict], details: dict = {}):
        super().__init__(message=message, details=details)
        if isinstance(exc, Exception):
            self.wrapped_exc = serialize(exc)
        elif isinstance(exc, dict):
            self.wrapped_exc = exc


class NotFoundWorkerError(BaseError):
    def __init__(self, service_name, target_type):
        super().__init__(f'Ready worker for {service_name} ({target_type}) is not found')


class WorkerError(WrapperError):
    pass


class SearchingError(WrapperError):
    pass


class NotUniqueUnitIdError(BaseError):
    def __init__(self, unit_id):
        super().__init__(f'Server already has client with unit_id {unit_id}. Unit_id must be unique.')


class ServiceNotSupportError(BaseError):
    def __init__(self, service_name):
        super().__init__(f'Service {service_name} is not supported')


class OverallTimeout(BaseError):
    def __init__(self):
        super().__init__('Overall timeout')


class UnknownError(WrapperError):
    pass


class PnValidationError(BaseError):
    pass
