phonenumbers
aiohttp==3.6.2
loguru==0.3.2
pymongo==3.9.0
websockets==8.1
Pillow==7.0.0
PyYAML==5.3
-e git+http://178.249.242.40:13150/BonoboGitServer/aiomsg.git#egg=aiomsg
